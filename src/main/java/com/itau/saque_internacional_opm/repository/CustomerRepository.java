package com.itau.saque_internacional_opm.repository;
import org.springframework.data.repository.CrudRepository;

import com.itau.saque_internacional_opm.controller.CustumerController;
import com.itau.saque_internacional_opm.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer,  Long>{

	public Customer findByUsername(String userName);
	
}
