package com.itau.saque_internacional_opm.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.saque_internacional_opm.model.Customer;
import com.itau.saque_internacional_opm.model.WithdrawRequest;
import com.itau.saque_internacional_opm.repository.CustomerRepository;
import com.itau.saque_internacional_opm.service.RatesConverterService;

@RestController
public class CustumerController {

	@Autowired
	private RatesConverterService ratesConverterService;
	
	@Autowired
	CustomerRepository customerRepository;

	@RequestMapping(method=RequestMethod.POST, path="/sacar")
	public Customer withdraw(@RequestBody WithdrawRequest withdrawRequest) {
		
		Double result = ratesConverterService.convert(withdrawRequest.getCurrency(), withdrawRequest.getAmount());
			
		Customer  customer = customerRepository.findByUsername(withdrawRequest.getUsername());
		customer.setBalance(customer.getBalance()-result);
		customerRepository.save(customer);
		return customer;
//		return CustomerRepository.save(customer);
	}
	
}
