package com.itau.saque_internacional_opm.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.saque_internacional_opm.model.RatesResponse;

@Service
public class RatesConverterService {
			
	public Double convert(String to, Double amount) {
				
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://data.fixer.io/api/latest?access_key=e539e11f4de63138df2cea4a75a4ad58&base=EUR";
		RatesResponse rates = restTemplate.getForObject(url, RatesResponse.class);
		return rates.getRates().get(to) * amount;
	}
	
}
